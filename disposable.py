import socket

from libtaskotron import check

def run(workdir=None):
    print "doing something trivial"
    cd = check.CheckDetail('disposable-test', check.ReportType.KOJI_BUILD, outcome = 'PASSED')
    cd.store("Executed on %s" % socket.gethostname())

    return check.export_TAP([cd])
